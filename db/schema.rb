# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_18_192708) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "event_host_workshops", force: :cascade do |t|
    t.bigint "workshop_id"
    t.bigint "event_host_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["event_host_id"], name: "index_event_host_workshops_on_event_host_id"
    t.index ["workshop_id"], name: "index_event_host_workshops_on_workshop_id"
  end

  create_table "event_hosts", force: :cascade do |t|
    t.string "fullname"
    t.string "major"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "payment_infos", force: :cascade do |t|
    t.integer "status", default: 0
    t.integer "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_workshop_id"
    t.index ["user_workshop_id"], name: "index_payment_infos_on_user_workshop_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "user_workshops", force: :cascade do |t|
    t.integer "user_id"
    t.integer "workshop_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  create_table "workshop_templates", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "duration"
    t.integer "price"
    t.integer "number_of_participants"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "workshops", force: :cascade do |t|
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "workshop_template_id"
  end

  add_foreign_key "users", "roles"
end
