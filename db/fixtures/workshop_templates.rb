WorkshopTemplate.seed(
  { id: 1, title: 'first', description: 'first description', duration: 120, price: 20, number_of_participants: 20 },
  { id: 2, title: 'second', description: 'second description', duration: 90, price: 50, number_of_participants: 30 },
  { id: 3, title: 'third', description: 'third description', duration: 150, price: 30, number_of_participants: 15 },
  { id: 4, title: 'fourth', description: 'fourth description', duration: 120, price: 20, number_of_participants: 20 }
)