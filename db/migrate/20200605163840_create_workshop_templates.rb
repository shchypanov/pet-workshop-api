class CreateWorkshopTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :workshop_templates do |t|
      t.string :title
      t.text :description
      t.integer :duration
      t.integer :price
      t.integer :number_of_participants

      t.timestamps
    end
  end
end
