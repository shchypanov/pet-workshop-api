class CreatePaymentInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_infos do |t|
      t.integer :status, default: 0
      t.integer :amount

      t.timestamps
    end
  end
end
