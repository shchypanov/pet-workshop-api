class EditWorkshopTemplateAndWorkshops < ActiveRecord::Migration[6.0]
  def change
    add_column :workshops, :workshop_template_id, :integer
    remove_column :workshop_templates, :workshop_id
  end
end
