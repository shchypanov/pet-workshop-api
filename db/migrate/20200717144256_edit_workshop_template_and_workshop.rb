class EditWorkshopTemplateAndWorkshop < ActiveRecord::Migration[6.0]
  def change
    remove_column :workshops, :workshop_template_id
    add_column :workshop_templates, :workshop_id, :integer
  end
end
