class CreateEventHosts < ActiveRecord::Migration[6.0]
  def change
    create_table :event_hosts do |t|
      t.string :fullname
      t.string :major

      t.timestamps
    end
  end
end
