class AddUserWorkshopIdToPaymentInfos < ActiveRecord::Migration[6.0]
  def change
    add_reference :payment_infos, :user_workshop, index: true
  end
end
