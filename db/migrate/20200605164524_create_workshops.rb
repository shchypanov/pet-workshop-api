class CreateWorkshops < ActiveRecord::Migration[6.0]
  def change
    create_table :workshops do |t|
      t.integer :workshop_template_id
      t.datetime :date

      t.timestamps
    end
  end
end
