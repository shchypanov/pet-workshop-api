class CreateEventHostWorkshops < ActiveRecord::Migration[6.0]
  def change
    create_table :event_host_workshops do |t|
      t.belongs_to :workshop
      t.belongs_to :event_host

      t.timestamps
    end
  end
end
