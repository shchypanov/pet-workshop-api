Jets.application.routes.draw do
  namespace :api do
    namespace :v1 do
      # Admin routes
      namespace :admin do
        resources :workshops, except: %i[index show]
        resources :workshop_templates
        resources :event_hosts, except: %i[show]
        resources :payment_infos, only: %i[index]
      end

      # Workshops
      resources :workshops, only: %i[index show]
      resources :user_workshops, only: %i[create] do
        resources :payments, only: %i[create]
        get 'success', to: 'payments#success', as: :payment_success
        get 'cancel', to: 'payments#cancel', as: :payment_cancel
      end

      # Payment

      # Authenticate
      resource :users, only: :create
      post 'login', to: 'users#login'
      post 'auto_login', to: 'users#auto_login'
      post 'set_new_modarator', to: 'users#set_new_modarator'
    end
  end

  # The jets/public#show controller can serve static utf8 content out of the public folder.
  # Note, as part of the deploy process Jets uploads files in the public folder to s3
  # and serves them out of s3 directly. S3 is well suited to serve static assets.
  # More info here: https://rubyonjets.com/docs/extras/assets-serving/
  any "*catchall", to: "jets/public#show"
end