if Jets.env == 'production'
  Jets.application.config.session :store, key: '_workshop_api', domain: 'eewkuev4rj.execute-api.eu-central-1.amazonaws.com/dev'
else
  Jets.application.config.session :store, key: '_workshop_api'
end