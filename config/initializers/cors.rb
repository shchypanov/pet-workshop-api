Jets.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'http://localhost:8888'
    resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options, :head], credentials: true
  end

  allow do
    origins 'https://eewkuev4rj.execute-api.eu-central-1.amazonaws.com/dev'
    resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options, :head], credentials: true
  end
end