class EmailBeforeWorkshopJob < ApplicationJob
  class_timeout 300

  rate "24 hours" # every 12 hours
  def send_before_workshop_email
    # check people's tomorrow workshops
    email = SendgridManager::SendBunchEmail.call(:send_before_workshop_email, collect_people)

    puts email
  end

  # test email sending
  rate "30 minutes"
  def send_email_every_minute
    email = SendgridManager::SendBunchEmail.call(:send_before_workshop_email, [['railsvtest@gmail.com', 'title 1', 'date 1'], ['vladshchypanov@gmail.com', 'title 2', 'date 2']])
    puts 'email sent'
    { email: email }
  end

  private

    def collect_people
      # Workshop.joins(:users, :workshop_template).where("date = ?", "2020-06-08 04:30:00").pluck(:email, :title)
      Workshop.joins(:users, :workshop_template).where(
        "date BETWEEN ? AND ?",
        DateTime.tomorrow.beginning_of_day,
        DateTime.tomorrow.end_of_day
      ).pluck(:email, :title, :date)
    end
end