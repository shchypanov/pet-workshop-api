class User < ApplicationRecord
  has_secure_password

  belongs_to :role, optional: true
  has_many :user_workshops
  has_many :workshops, through: :user_workshops

  validates_presence_of :email
  validates_uniqueness_of :email

  def modarator!
    update(role_id: 2)
  end
end
