class Workshop < ApplicationRecord
  belongs_to :workshop_template
  has_many :user_workshops
  has_many :users, through: :user_workshops
  has_many :event_host_workshops
  has_many :event_hosts, through: :event_host_workshops
end
