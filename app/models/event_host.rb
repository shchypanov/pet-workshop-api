class EventHost < ApplicationRecord
  has_many :event_host_workshops
  has_many :workshops, through: :event_host_workshops
end
