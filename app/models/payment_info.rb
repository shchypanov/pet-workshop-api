class PaymentInfo <  ApplicationRecord
  belongs_to :user_workshop

  enum status: %i[
    unpaid
    fully_paid
    partially_paid
    cash_payment
  ]
end