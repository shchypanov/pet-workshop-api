class UserWorkshop < ApplicationRecord
  belongs_to :user
  belongs_to :workshop
  has_one :payment_info
end
