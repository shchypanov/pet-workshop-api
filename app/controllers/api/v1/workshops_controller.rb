module Api
  module V1
    class WorkshopsController < ApplicationController
      before_action :set_workshop, only: [:show]

      def index
        @workshops = Workshop.all

        render json: @workshops
      end

      def show
        render json: @workshop
      end

      private
        def set_workshop
          @workshop = Workshop.find(params[:id])
        end
    end
  end
end