module Api
  module V1
    class PaymentsController < ApplicationController
      before_action :authorized, only: %i[
        create
        success
        cancel
      ]
      before_action :set_workshop_template, only: %i[
        create
        success
      ]

      def create
        if @workshop_template.nil?
          render json: { error: 'workshop is nil' }
          return
        end

        if cash_payment
          set_payment_info
          render json: user_workshop.payment_info
        else
          session = set_session

          # TODO: Frontend
          # const stripe = Stripe(publishable_key);

          # stripe.redirectToCheckout({
          #   sessionId: session_id
          # }).then (function (result) {
          #   console.log(result.error.message);
          # });
          if session[:success]
            render json: {
              publishable_key: Jets.application.config.stripe[:publishable_key],
              session_id: session[:session].id
            }
          else
            render json: {
              error: session[:error]
            }
          end
        end
      end

      def success
        set_payment_info
        email = SendgridManager::SendEmail.call(:send_after_booking_email, @user)

        session = Stripe::Checkout::Session.retrieve(params[:session_id])
        payment_intent = Stripe::PaymentIntent.retrieve(session.payment_intent)
        render json: { session: session, payment_intent: payment_intent, email_status: email[:status] }
      end

      def cancel
        render json: 'Payment session was unsuccess!'
      end

      private

        def set_session
          StripeManager::MakeCheckout.call(
            @workshop_template,
            params[:amount],
            payment_success_url(params[:user_workshop_id]),
            payment_cancel_url(params[:user_workshop_id])
          )
        end

        def set_workshop_template
          @workshop_template = user_workshop&.workshop.workshop_template
        end

        def set_payment_info
          user_workshop.payment_info = PaymentInfo.new(status: set_payment_status, amount: params[:amount])
        end

        def user_workshop
          UserWorkshop.find(params[:user_workshop_id])
        end

        def set_payment_status
          return 1 if params[:amount] == @workshop_template.price
          return 3 if cash_payment

          2
        end

        def cash_payment
          params[:amount] == '0'
        end
    end
  end
end