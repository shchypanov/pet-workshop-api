module Api
  module V1
    class UsersController < ApplicationController
      before_action :authorized, only: %i[
        auto_login
        set_new_modarator
      ]

      # REGISTER
      def create
        @user = User.create(user_params)
        set_default_role
        if @user.valid?
          token = encode_token({user_id: @user.id})
          render json: {user: @user, token: token}
        else
          render json: {error: "Invalid username or password"}
        end
      end

      # LOGGING IN
      def login
        @user = User.find_by(email: params[:user][:email])
        if @user && @user.authenticate(params[:user][:password])
          token = encode_token({user_id: @user.id})
          render json: { user: @user, token: token }
        else
          render json: { error: "Invalid username or password" }
        end
      end

      def auto_login
        render json: @user
      end

      def set_new_modarator
        new_modarator ||= User.find_by_email(params[:user][:email])
        if admin && new_modarator
          new_modarator.modarator!
          render json: { user: new_modarator }
        else
          render json: { error: new_modarator.nil? ? 'Wrong user email' : 'Something went wrong!' }
        end
      end

      private

      def user_params
        params.require(:user).permit(:email, :password, :password_confirmation)
      end

      def set_default_role
        @user.update(role_id: 1)
      end
    end
  end
end