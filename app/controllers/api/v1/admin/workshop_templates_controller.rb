module Api
  module V1
    module Admin
      class WorkshopTemplatesController < ApplicationController
        before_action :admin_group_session
        before_action :set_workshop_template, only: [:show, :update, :delete]

        def index
          @workshop_templates = WorkshopTemplate.all

          render json: @workshop_templates
        end

        def show
          render json: @workshop_template
        end

        def create
          workshop_template = WorkshopTemplate.new(workshop_template_params)

          if workshop_template.save
            render json: workshop_template, status: :created
          else
            render json: workshop_template.errors, status: :unprocessable_entity
          end
        end

        def update
          if @workshop_template.update(workshop_template_params)
            render json: @workshop_template
          else
            render json: @workshop_template.errors, status: :unprocessable_entity
          end
        end

        def delete
          @workshop_template.destroy
          render json: {deleted: true}
        end

        private
          def set_workshop_template
            @workshop_template = WorkshopTemplate.find(params[:id])
          end

          def workshop_template_params
            params.require(:workshop_template).permit(:title, :description, :duration, :price, :number_of_participants)
          end
      end
    end
  end
end