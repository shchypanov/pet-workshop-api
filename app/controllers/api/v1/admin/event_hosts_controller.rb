module Api
  module V1
    module Admin
      class EventHostsController < ApplicationController
        before_action :admin_group_session
        before_action :set_event_host, only: %i[
          update
          delete
        ]

        def index
          event_hosts = EventHost.all

          render json: event_hosts
        end

        def create
          event_host = EventHost.new(event_host_params)

          if event_host.save
            render json: event_host, status: :created
          else
            render json: event_host.errors, status: :unprocessable_entity
          end
        end

        def update
          if @event_host.update(event_host_params)
            render json: @event_host
          else
            render json: @event_host.errors, status: :unprocessable_entity
          end
        end

        def delete
          @event_host.destroy
          render json: {deleted: true}
        end

        private
          def set_event_host
            @event_host = EventHost.find(params[:id])
          end

          def event_host_params
            params.require(:event_host).permit(:fullname, :major)
          end
      end
    end
  end
end
