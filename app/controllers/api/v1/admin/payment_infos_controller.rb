module Api
  module V1
    module Admin
      class PaymentInfosController < ApplicationController
        before_action :admin_group_session

        def index
          payment_infos = PaymentInfo.all

          render json: payment_infos.includes(:user_workshop)
        end
      end
    end
  end
end