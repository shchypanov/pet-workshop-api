module Api
  module V1
    module Admin
      class WorkshopsController < ApplicationController
        before_action :admin_group_session

        before_action :set_workshop, only: %i[
          update
          delete
          add_event_hosts
        ]

        def create
          workshop = Workshop.new(workshop_params)

          if workshop.save
            render json: workshop, status: :created
          else
            render json: workshop.errors, status: :unprocessable_entity
          end
        end

        def update
          if @workshop.update(workshop_params)
            render json: @workshop
          else
            render json: @workshop.errors, status: :unprocessable_entity
          end
        end

        def delete
          @workshop.destroy
          render json: {deleted: true}
        end

        private
          def set_workshop
            @workshop = Workshop.find(params[:id])
          end

          def workshop_params
            params.require(:workshop).permit(:workshop_template_id, :date)
          end
      end
    end
  end
end