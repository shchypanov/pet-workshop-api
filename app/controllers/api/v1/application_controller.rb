module Api
  module V1
    class ApplicationController < Jets::Controller::Base
      # before_action :authorized

      def encode_token(payload)
        JWT.encode(payload, ENV['SECRET_KEY_BASE'])
      end

      def auth_header
        # { Authorization: 'Bearer <token>' }
        request.headers['authorization']
      end

      def decoded_token
        if auth_header
          token = auth_header.split(' ')[1]
          # header: { 'Authorization': 'Bearer <token>' }
          begin
            JWT.decode(token, ENV['SECRET_KEY_BASE'], true, algorithm: 'HS256')
          rescue JWT::DecodeError
            nil
          end
        end
      end

      def logged_in_user
        if decoded_token
          user_id = decoded_token[0]['user_id']
          @user = User.find_by(id: user_id)
        end
      end

      def logged_in
        !!logged_in_user
      end

      def authorized
        render json: { message: 'Please log in' }, status: :unauthorized unless logged_in
      end

      def admin_group_session
        if !logged_in
          return render json: { message: 'Please log in' }, status: :unauthorized
        elsif logged_in && !admin_group
          render json: { message: 'You should be an admin!' }, status: :unauthorized
        end
      end

      def admin
        logged_in_user.role.code == 'admin'
      end

      def modarator
        logged_in_user.role.code == 'modarator'
      end

      def admin_group
        admin || modarator
      end
    end
  end
end