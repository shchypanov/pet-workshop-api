module Api
  module V1
    class UserWorkshopsController < ApplicationController
      before_action :authorized, only: [:create]

      def create
        user_workshop = UserWorkshop.create!(
          user_id: @user.id,
          workshop_id: params[:workshop_id]
        )
        if user_workshop.save
          render json: user_workshop, status: :created
        else
          render json: user_workshop.errors, status: :unprocessable_entity
        end
      end
    end
  end
end