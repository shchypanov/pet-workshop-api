
module SendgridManager
  class SendEmail < ApplicationService
    require 'sendgrid-ruby'
    include SendGrid

    def initialize(event, user)
      @event       = event
      @user        = user
    end

    def call
      begin
        response = send(@event)
        return { status: 'sent', response: response }
      rescue Exception => error
        error_message(error)
      end
    end

    private

    def error_message(error)
      "Sendgrid email delivery failure with: #{error} \n Backtrace: #{error.backtrace}"
    end

    def prepare_email(subject, body)
      from    = Email.new(email: 'shchipanovv@gmail.com')
      to      = Email.new(email: @user.email)
      content = Content.new(
        type: 'text/plain',
      )
      content.value = body
      Mail.new(from, subject, to, content)
    end

    def send_email(mail)
      sg = sg_auth
      sg.client.mail._('send').post(request_body: mail.to_json)
    end

    def sg_auth
      SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    end

    # events
    def send_after_booking_email
      subject = "Thank you, #{@user.email}"
      body = "You have successfully booked a workshop!"
      send_email( prepare_email(subject, body) )
    end
  end
end