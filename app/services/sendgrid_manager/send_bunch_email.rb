
module SendgridManager
  class SendBunchEmail < ApplicationService
    require 'sendgrid-ruby'
    include SendGrid

    attr_accessor :event, :users_bunch

    TEMPLATES = {
      send_before_workshop_email: 'd-1e12cdd7c4c9424495019b86756b5ba8'
    }

    def initialize(event, users_bunch)
      @event       = event
      @users_bunch = users_bunch
      @users_data  = []
      @user_data   = user_data
    end

    def call
      url = URI('https://api.sendgrid.com/v3/mail/send')
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = create_request(url)
      response = http.request(request)

      if response_success?(response)
        return { status: 'sent', response: response }
      end

      { status: "didn't sent", response: error_message(response) }
    end

    private

    def error_message(response)
      "Sendgrid email delivery failure with: #{response}"
    end

    def user_data
      return { success: false } if users_bunch.empty?

      users_bunch.map do |user|
        @users_data << {
          to: [{
            email: user[0]
          }
          ],
          dynamic_template_data: {
            name: user[0],
            workshop_name: user[1],
            workshop_time: user[2]
          }
        }
      end
    end

    def create_data_mail
      {
        personalizations: @users_data,
        from: {
          email: 'shchipanovv@gmail.com',
          name: 'WorkShop'
        },
        template_id: TEMPLATES[event]
      }
    end

    def create_request(url)
      request = Net::HTTP::Post.new(url)
      request['authorization'] = "Bearer #{ENV['SENDGRID_API_KEY']}"
      request['content-type'] = 'application/json'
      request.body = create_data_mail.to_json
      request
    end

    def response_success?(response)
      response.code[0] == '2'
    end
  end
end
