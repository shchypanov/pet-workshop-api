module StripeManager
  class MakeCheckout < ApplicationService
    attr_accessor :title, :description, :amount, :success_url, :cancel_url

    def initialize(workshop_template, amount, success_url, cancel_url)
      @title       = workshop_template.title
      @description = workshop_template.description
      @amount      = amount
      @success_url = success_url
      @cancel_url  = cancel_url
    end

    def call
      begin
        {
          success: true,
          session: set_session
        }
      rescue Stripe::RateLimitError,
             Stripe::InvalidRequestError,
             Stripe::AuthenticationError,
             Stripe::APIConnectionError,
             Stripe::StripeError => e
        {
          success: false,
          error: set_session #set .error
        }
      end
    end

    private

     def set_session
       # Setting up a Stripe Session for payment.
       Stripe::Checkout::Session.create(
        payment_method_types: ['card'],
        line_items: [{
          name: title,
          description: description,
          amount: amount.to_i * 1000,
          currency: 'usd',
          quantity: 1
        }],
        success_url: success_url + '?session_id={CHECKOUT_SESSION_ID}',
        cancel_url: cancel_url
      )
     end
  end
end