class ApplicationService

  UNKNOWN_ERROR = 'An error was occured. Please contact us.'.freeze

  def self.call (*args, &block)
    new(*args, &block).call
  end
end
